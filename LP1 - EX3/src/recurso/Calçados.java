package recurso;

import java.util.Scanner;

public class Cal�ados {
	
	private String marca;
	private String nome;
	private int tamanho;
	private String cor;
	
	public Cal�ados(String marca, String nome, int tamanho, String cor) {
		super();
		this.marca = marca;
		this.nome = nome;
		this.tamanho = tamanho;
		this.cor = cor;
	}
	
	public String getMarca() {
		return marca;
	}
	public void setMarca(String marca) {
		this.marca = marca;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public int getTamanho() {
		return tamanho;
	}
	public void setTamanho(int tamanho) {
		this.tamanho = tamanho;
	}
	public String getCor() {
		return cor;
	}
	public void setCor(String cor) {
		this.cor = cor;
	}
	
	public void dados() {
		System.out.println("==============================================================");
		System.out.println("==================== Detalhes dos Cal�ados ===================");
		System.out.println("==============================================================");
		System.out.println("");
		System.out.println("Nome: " + nome);
		System.out.println("Marca: " + marca);
		System.out.println("Tamanho: " + tamanho);
		System.out.println("Cor: " + cor);
		System.out.println("");
	}
	
	public void comprar() {
		System.out.println("=================== Processo de compra de cal�ados ==================");
		System.out.println("");
		System.out.println("Comprando o "+nome + cor);
		System.out.println("10%");
		System.out.println("--------- 40%");
		System.out.println("------------------ 80%");
		System.out.println("--------------------------- 100%");
		System.out.println("Pronto, agora posso usar meu but novo!");
		System.out.println("");
	}
	
	public void cal�ar() {
		System.out.println("=================== Cal�ando o tenis novo... ==================");
		System.out.println("");
		System.out.println("Cal�ando o but novo...");
		System.out.println("");
	}
	
	public static void main(String[] args) {
		Cal�ados but1 = new Cal�ados("Nike", "Jordan 1", 44, "Vermelho e branco");
		Cal�ados but2 = new Cal�ados("Nike", "Jordan 4", 43, "Preto, cinza e verde");
		Cal�ados but3 = new Cal�ados("Adidas", "OZWEEGO", 42, "Branco");
		
		Scanner myobj = new Scanner(System.in);
	    String Cal�ados;
	    
	    // Enter username and press Enter
	    System.out.println("Escolha um tenis (Jordan 1, Jordan 4 ou OZWEEGO): "); 
	    Cal�ados = myobj.nextLine();
	    
		
	    if(but1.getNome().equals(Cal�ados)) {
	    	but1.dados();
	    	but1.comprar();
	    	but1.cal�ar();
	    }
	    
	    if(but2.getNome().equals(Cal�ados)) {
	    	but2.dados();
	    	but2.comprar();
	    	but2.cal�ar();
	    }
	    
	    if(but3.getNome().equals(Cal�ados)) {
	    	but3.dados();
	    	but3.comprar();
	    	but3.cal�ar();
	    }
	}
}