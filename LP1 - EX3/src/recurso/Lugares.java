package recurso;

public class Lugares {
	
	private String cidade;
	private String pais;
	private String temperatura;
	private String ambiente;
	private String cartaoPostal;
	
	 
	public Lugares(String cidade, String pais, String temperatura, String ambiente, String cartaoPostal) {
		super();
		this.cidade = cidade;
		this.pais = pais;
		this.temperatura = temperatura;
		this.ambiente = ambiente;
		this.cartaoPostal = cartaoPostal;
	}
	
	public String getCidade() {
		return cidade;
	}
	
	public void setCidade(String cidade) {
		this.cidade = cidade;
	}
	
	public String getPais() {
		return pais;
	}
	
	public void setPais(String pais) {
		this.pais = pais;
	}
	
	public String getTemperatura() {
		return temperatura;
	}
	
	public void setTemperatura(String temperatura) {
		this.temperatura = temperatura;
	}
	
	public String getAmbiente() {
		return ambiente;
	}

	public void setAmbiente(String ambiente) {
		this.ambiente = ambiente;
	}

	public String getCartaoPostal() {
		return cartaoPostal;
	}

	public void setCartaoPostal(String cartaoPostal) {
		this.cartaoPostal = cartaoPostal;
	}
	
	public void localiza��o() {
		System.out.println("=========================================================");
		System.out.println("================== Localiza��o da Cidade ================");
		System.out.println("=========================================================");
		
		System.out.println("Pais: "+ pais);
		System.out.println("Cidade: "+ cidade);
	}
	
	public void detalhesLugar() {
		System.out.println("=========================================================");
		System.out.println("=================== Detalhes da Cidade ==================");
		System.out.println("=========================================================");
		System.out.println("Ambiente: "+ ambiente);
		System.out.println("Temperatura Ambiente: "+ temperatura);
		System.out.println("Cart�o Postal: "+ cartaoPostal);
	}
	
	public void viajar() {
		System.out.println("=================== Bora viajar! ==================");
		System.out.println("");
		System.out.println("Agora que eu conhe�o o lugar, vou viajar!");
	}
	
	public static void main(String[] args) {
		Lugares I=new Lugares("Italia","Paris","18 �C","Interior de Paris","Torre Eiffel");
		Lugares C=new Lugares("California","EUA","24 �C","Litoral dos EUA","Golden Gate");
		
		I.localiza��o();
		I.detalhesLugar();
		I.viajar();
		C.localiza��o();
		C.detalhesLugar();
		C.viajar();
		}
	
}
