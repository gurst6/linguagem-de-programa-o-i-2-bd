package recurso;

import java.util.Scanner;

public class PessoaCaracter {
	
	private String nome;
	private char sexo;
	private int idade;
	private String assento;
	

	public PessoaCaracter(String nome, char sexo, int idade, String assento) {
		super();
		this.nome = nome;
		this.sexo = sexo;
		this.idade = idade;
		this.assento = assento;
	}
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public char getSexo() {
		return sexo;
	}
	public void setSexo(char sexo) {
		this.sexo = sexo;
	}
	public int getIdade() {
		return idade;
	}
	public void setIdade(int idade) {
		this.idade = idade;
	}
	
	public String getAssento() {
		return assento;
	}

	public void setAssento(String assento) {
		this.assento = assento;
	}
	
	public void dados() {
		
		System.out.println("=========================================================");
		System.out.println("============== IDENTIFICADOR DE PASSAGEIROS =============");
		System.out.println("=========================================================");
		
		System.out.println("Nome: " + nome);
		System.out.println("Sexo do passageiro: " + sexo);
		System.out.println("idade do passageiro: " + idade);
	}
	
	public void assento() 
	
	{
		System.out.println("Assento do passageiro: " + assento);
	}
	public void maioridade() {
		
		if(idade < 18) {
			System.out.println("Obs.: O passagerio � menor de idade, deve viajar com acompanhante ou apresentar uma autoriza��o!");
		} else {
			System.out.println("Obs.: O passageiro � maior de idade, pode viajar sozinho!");
		}
	}
	
	
	
	public static void main(String[] args) {
		PessoaCaracter passageiro1 = new PessoaCaracter("Gustavo", 'M', 17,"B12");
		PessoaCaracter passageiro2 = new PessoaCaracter("Ana", 'F', 20,"A20");
		PessoaCaracter passageiro3 = new PessoaCaracter("Leandro", 'M',35, "F15");
		
		Scanner myobj = new Scanner(System.in);
	    String Passageiro;
	    
	    // Enter username and press Enter
	    System.out.println("Insira o nome do passagerio abaixo:"); 
	    Passageiro = myobj.nextLine();
	    
		
	    if(passageiro1.getNome().equals(Passageiro)) {
	    	passageiro1.dados();
	    	passageiro1.assento();
	    	passageiro1.maioridade();
	    } else {
	    	
	    	if(passageiro2.getNome().equals(Passageiro)) {
	    	passageiro2.dados();
	    	passageiro2.assento();
	    	passageiro2.maioridade();
	    	} else { 
	    		
			    if(passageiro3.getNome().equals(Passageiro)) {
			    	passageiro3.dados();
			    	passageiro3.assento();
			    	passageiro3.maioridade();
			    	} else {
			    		System.out.println("=========================================================");
			    		System.out.println("============== IDENTIFICADOR DE PASSAGEIROS =============");
						System.out.println("=========================================================");
						
				    	System.out.println("O passageiro n�o encontrado no banco de dados!");
			    	}
			
	    		}
	    }
	}
}
