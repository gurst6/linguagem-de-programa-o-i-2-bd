package recurso;

import java.util.Scanner;

public class Profissional {
	
	private String nome;
	private String cargo;
	private String salario;
	private String gradua��o;
	private String area;
	private String empresa;
	
	public Profissional(String nome, String cargo, String salario, String gradua��o, String area, String empresa) {
		super();
		this.nome = nome;
		this.cargo = cargo;
		this.salario = salario;
		this.gradua��o = gradua��o;
		this.area = area;
		this.empresa = empresa;
	}
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCargo() {
		return cargo;
	}
	public void setCargo(String cargo) {
		this.cargo = cargo;
	}
	public String getSalario() {
		return salario;
	}
	public void setSalario(String salario) {
		this.salario = salario;
	}
	public String getGradua��o() {
		return gradua��o;
	}
	public void setGradua��o(String gradua��o) {
		this.gradua��o = gradua��o;
	}
	public String getArea() {
		return area;
	}
	public void setArea(String area) {
		this.area = area;
	}
	public String getEmpresa() {
		return empresa;
	}
	public void setEmpresa(String empresa) {
		this.empresa = empresa;
	}
	public void dados() {
		System.out.println("==============================================================");
		System.out.println("==================== Sistema de RH - Dados ===================");
		System.out.println("==============================================================");
		System.out.println("");
		System.out.println("Nome: " + nome);
		System.out.println("Cargo: " + cargo);
		System.out.println("Salario: " + salario);
		System.out.println("Gradua��o: " + gradua��o);
		System.out.println("�rea: " + area);
		System.out.println("Nome da Empresa: " + empresa);
		System.out.println("");
	}
	public void demissao() { 
		System.out.println("=================== Processo de Demiss�o de Funcionario ==================");
		System.out.println("");
		System.out.println("Iniciando o processo de demiss�o do funcionario "+nome+" no valor de "+ salario+".");
		System.out.println("10%");
		System.out.println("--------- 40%");
		System.out.println("------------------ 80%");
		System.out.println("--------------------------- 100%");
		System.out.println("Demiss�o finalizada, o funcionario n�o est� mais vinculado a essa empresa!");
		System.out.println("");
	}
	
	public void pagSalario() { 
		System.out.println("=================== Processo de Pagamento de Salario ==================");
		System.out.println("");
		System.out.println("Iniciando o pagamento do salario do funcionario "+nome+" no valor de "+ salario+".");
		System.out.println("10%");
		System.out.println("--------- 40%");
		System.out.println("------------------ 80%");
		System.out.println("--------------------------- 100%");
		System.out.println("Pagamento finalizado, o salario j� est� disponivel para o funcionario!");
		System.out.println("");
	}
	
	public static void main(String[] args) {
		Profissional profissional1 = new Profissional("Gustavo Ribeiro", "Analista de IT", "R$5.000", "Banco de dados", "IT", "Google");
		Profissional profissional2 = new Profissional("Ana Claro", "Ajudante de RH", "R$3.000", "Administra��o", "RH", "Embraer");
		Profissional profissional3 = new Profissional("Lucas Silva", "Estagiario", "R$1.500", "Engenharia de Processo", "Manuten��o", "Johnson & Johnson");
		
		Scanner myobj = new Scanner(System.in);
	    String Profissionais;
	    
	    // Enter username and press Enter
	    System.out.println("Insira o nome do funcionario: "); 
	    Profissionais = myobj.nextLine();
	    
		
	    if(profissional1.getNome().equals(Profissionais)) {
	    	profissional1.dados();
	    	profissional1.pagSalario();
	    	profissional1.demissao();
	    }
	    
	    if(profissional2.getNome().equals(Profissionais)) {
	    	profissional2.dados();
	    	profissional2.pagSalario();
	    	profissional2.demissao();
	    }
	    
	    if(profissional3.getNome().equals(Profissionais)) {
	    	profissional3.dados();
	    	profissional3.pagSalario();
	    	profissional3.demissao();
	    }
		
	}

	
}
