package recurso;

import java.util.Scanner;

public class Instrumentos_Musicais {
	
	private String nome;
	private String tipo;
	private char Cordas;
	private int NdeCordas;
	private String material;
	private String fabricante;	
	
	public Instrumentos_Musicais(String nome, String tipo, char cordas, int ndeCordas, String material,String fabricante) {
		super();
		this.nome = nome;
		this.tipo = tipo;
		Cordas = cordas;
		NdeCordas = ndeCordas;
		this.material = material;
		this.fabricante = fabricante;
	}
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public char getCordas() {
		return Cordas;
	}
	public void setCordas(char cordas) {
		Cordas = cordas;
	}
	public int getNdeCordas() {
		return NdeCordas;
	}
	public void setNdeCordas(int ndeCordas) {
		NdeCordas = ndeCordas;
	}
	public String getMaterial() {
		return material;
	}
	public void setMaterial(String material) {
		this.material = material;
	}
	public String getFabricante() {
		return fabricante;
	}
	public void setFabricante(String fabricante) {
		this.fabricante = fabricante;
	}
	
	public void dados() {
		System.out.println("==============================================================");
		System.out.println("=================== Detalhes do Instrumento ==================");
		System.out.println("==============================================================");
		System.out.println("Nome do instrumento: " + nome);
		System.out.println("Tipo de instrumento: " + tipo);
		
		if(Cordas == 'S' ) {
			System.out.println("Numero de Cordas: " + NdeCordas);
		}
		System.out.println("Material do instrumento: " + material);
		System.out.println("Fabricante do instrumento: " + fabricante);
		System.out.println("");
	}
	
	public void afinando() {
		System.out.println("==============================================================");
		System.out.println("Eu estou afinando o "+nome+"...");
		System.out.println("10%");
		System.out.println("--------- 40%");
		System.out.println("------------------ 80%");
		System.out.println("--------------------------- 100%");
		System.out.println("Finalizado, o instrumento est� pronto para ser usado!");
	}
	
	public void tocando() {
		System.out.println("Eu estou tocando "+ nome + " agora!");
	}
	
	public static void main(String[] args) {
		Instrumentos_Musicais instrumento1 = new Instrumentos_Musicais("Violao", "Instrumento de Corda", 'S', 6, "Madeira", "Yamaha");
		Instrumentos_Musicais instrumento2 = new Instrumentos_Musicais("Tambor", "Instrumento de Percuss�o", 'N', 0, "Madeira e abada", "Alibada");
		Instrumentos_Musicais instrumento3 = new Instrumentos_Musicais("Pandeiro", "Instrumento de Percuss�o", 'N', 0, "Madeira e plastico", "Alibada");
		Instrumentos_Musicais instrumento4 = new Instrumentos_Musicais("Cavaquinho", "Instrumento de Corda", 'S', 4, "Madeira", "Yamaha");
		
		Scanner myobj = new Scanner(System.in);
	    String Instrumentos;
	    
	    // Enter username and press Enter
	    System.out.println("Escolha um instrumento musical (Violao, Tambor, Pandeiro ou Cavaquinho): "); 
	    Instrumentos = myobj.nextLine();
	    
		
	    if(instrumento1.getNome().equals(Instrumentos)) {
	    	instrumento1.dados();
	    	instrumento1.afinando();
	    	instrumento1.tocando();
	    }
	    
	    if(instrumento2.getNome().equals(Instrumentos)) {
	    	instrumento2.dados();
	    	instrumento2.afinando();
	    	instrumento2.tocando();
	    }
	    
	    if(instrumento3.getNome().equals(Instrumentos)) {
	    	instrumento3.dados();
	    	instrumento3.afinando();
	    	instrumento3.tocando();
	    }
	    
	    if(instrumento4.getNome().equals(Instrumentos)) {
	    	instrumento4.dados();
	    	instrumento4.afinando();
	    	instrumento4.tocando();
	    }		
	}
}
