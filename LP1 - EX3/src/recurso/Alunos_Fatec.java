package recurso;

import java.util.Scanner;

public class Alunos_Fatec {
	
	private String nome;
	private char sexo;
	private String curso;
	private int semestre;
	
	public Alunos_Fatec(String nome, char sexo, String curso, int semestre) {
		super();
		this.nome = nome;
		this.sexo = sexo;
		this.curso = curso;
		this.semestre = semestre;
	}
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public char getSexo() {
		return sexo;
	}
	public void setSexo(char sexo) {
		this.sexo = sexo;
	}
	public String getCurso() {
		return curso;
	}
	public void setCurso(String curso) {
		this.curso = curso;
	}
	public int getSemestre() {
		return semestre;
	}
	public void setSemestre(int semestre) {
		this.semestre = semestre;
	}
	
	public void dados() {
		System.out.println("==============================================================");
		System.out.println("====================== Detalhes do Aluno =====================");
		System.out.println("==============================================================");
		System.out.println("");
		System.out.println("Nome: " + nome);
		System.out.println("Sexo: " + sexo);
		System.out.println("Curso: " + curso);
		System.out.println("Semestre: " + semestre);
		System.out.println("");
	}
	
	public void NovoAluno() {
		System.out.println("=================== Processo de Cadastro do Novos Alunos ==================");
		System.out.println("");
		System.out.println("Iniciando o cadastro do aluno: "+ nome +" no curso de "+ curso +".");
		System.out.println("10%");
		System.out.println("--------- 40%");
		System.out.println("------------------ 80%");
		System.out.println("--------------------------- 100%");
		System.out.println("Aluno cadastrado!");
		System.out.println("");
	}
	
	public void conclus�o() {
		System.out.println("=================== Processo de Conclus�o de Curso ==================");
		System.out.println("");
		System.out.println("Finalizando o cadastro do aluno: "+ nome +" no curso de "+ curso +".");
		System.out.println("10%");
		System.out.println("--------- 40%");
		System.out.println("------------------ 80%");
		System.out.println("--------------------------- 100%");
		System.out.println("Aluno concluiu o curso!");
		System.out.println("");
	}
	
	public static void main(String[] args) {
		Alunos_Fatec aluno1 = new Alunos_Fatec("Gustavo Ribeiro", 'M', "Banco de dados", 2);
		Alunos_Fatec aluno2 = new Alunos_Fatec("Ana Clara", 'F', "ADS", 1);
		Alunos_Fatec aluno3 = new Alunos_Fatec("Luiz Gabriel", 'M', "Banco de dados", 2);
		
		Scanner myobj = new Scanner(System.in);
	    String Alunos;
	    
	    // Enter username and press Enter
	    System.out.println("Insira o nome do aluno: "); 
	    Alunos = myobj.nextLine();
	    
		
	    if(aluno1.getNome().equals(Alunos)) {
	    	aluno1.dados();
	    	aluno1.NovoAluno();
	    	aluno1.conclus�o();
	    }
	    
	    if(aluno2.getNome().equals(Alunos)) {
	    	aluno2.dados();
	    	aluno2.NovoAluno();
	    	aluno2.conclus�o();
	    }
	    
	    if(aluno3.getNome().equals(Alunos)) {
	    	aluno3.dados();
	    	aluno3.NovoAluno();
	    	aluno3.conclus�o();
	    }
		
	}
}
