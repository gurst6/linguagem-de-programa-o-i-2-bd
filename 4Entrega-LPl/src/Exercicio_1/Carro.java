package Exercicio_1;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Carro {
	
	private String carro;
	private String valor;
	private String placa;
	
	public Carro(String carro, String valor, String placa) {
		super();
		this.carro = carro;
		this.valor = valor;
		this.placa = placa;
	}
	
	public String getCarro() {
		return carro;
	}
	public void setCarro(String carro) {
		this.carro = carro;
	}
	public String getValor() {
		return valor;
	}
	public void setValor(String valor) {
		this.valor = valor;
	}
	
	public String getPlaca() {
		return placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}
	
	public void ListCars() {
		ArrayList<String> cars = new ArrayList<String>();
		cars.add(carro);
		System.out.println("Livro de carros: " + cars.toString());
	}
	
	public void TableValue() {
		ArrayList<String> value = new ArrayList<String>();
		value.add(valor);
		System.out.println("Livro do valores: " + value.toString());
	}
	
	public void ListarDados(String placa) {
		Map<String, String> nomes = new HashMap<>();
		
		nomes.put("AAA-1111", "BMW");
		nomes.put("BBB-2222", "Mercedes");
		
		Map<String, String> valores = new HashMap<>();
		
		valores.put("AAA-1111", "1000 conto");
		valores.put("BBB-2222", "5000 conto");
		
		System.out.println("==============================================================");
		System.out.println("====================== Dados do Veiculo ======================");
		System.out.println("==============================================================");

        System.out.println("Nome do carro: " + nomes.get(placa));
        System.out.println("Valor do carro: " + valores.get(placa));         
	}
	
	public static void main(String[] args) {
		Carro car1 = new Carro("BMW","1000 conto","AAA-1111");	  
		Carro car2 = new Carro("Mercedes","5000 conto","BBB-2222");	  
	    car1.ListCars();
	    car2.ListCars();
	    
	    car1.TableValue();
	    car2.TableValue();
	    
		Scanner myobj = new Scanner(System.in);
	    String numero;
	    
	    // Enter username and press Enter
	    System.out.println("Insira a placa do carro: "); 
	    numero = myobj.nextLine();

	    car1.ListarDados(numero);
	}
	
}
