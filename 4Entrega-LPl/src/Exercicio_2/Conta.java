package Exercicio_2;
import java.util.ArrayList;

public class Conta {
	
	private String titular;
	private int saldo;

	public Conta(String titular, int saldo) {
		super();
		this.titular = titular;
		this.saldo = saldo;
	}
	
	public String getTitular() {
		return titular;
	}
	public void setTitular(String titular) {
		this.titular = titular;
	}

	public int getSaldo() {
		return saldo;
	}
	public void setSaldo(int saldo) {
		this.saldo = saldo;
	}
	
	public static ArrayList<Conta> contas = new ArrayList<>();

	public void addConta(Conta conta1, Conta conta2, Conta conta3) {
		// TODO Auto-generated method stub
		contas.add(conta1);
		contas.add(conta2);
		contas.add(conta3);
		
		for(int i=0; i<contas.size(); i++) {
			System.out.println("======== Dados da Conta ========");
			System.out.println("Nome: " + contas.get(i).getTitular() + "\n" + "Saldo: " + contas.get(i).getSaldo());
		}
	}
	
}
