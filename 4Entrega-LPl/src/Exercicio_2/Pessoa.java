package Exercicio_2;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class Pessoa {
	
	private String nome;
	private String cidade;
	private String cpf;
	
	public Pessoa(String nome, String cidade, String cpf) {
		super();
		this.nome = nome;
		this.cidade = cidade;
		this.cpf = cpf;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCidade() {
		return cidade;
	}
	public void setCidade(String cidade) {
		this.cidade = cidade;
	}
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	
	Set<Pessoa> pessoas = new HashSet<>();
	
	public void ListarDadosPessoa(Pessoa pessoa1, Pessoa pessoa2, Pessoa pessoa3) {
		
		pessoas.add(pessoa1);
		pessoas.add(pessoa2);
		pessoas.add(pessoa3);
		
		for(Pessoa p : pessoas) {
			System.out.println("======== Dados da Pessoa ========");
			System.out.println("Nome: " + p.getNome() + "\n" + "Cidade: " + p.getCidade() + "\n" + "CPF: " + p.getCpf());
		}
	}
}
